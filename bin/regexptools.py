import re


def regexpMatchFunction(regexp):
    return lambda s : bool(re.compile(regexp).search(s))

def regexpInverseMatchFunction(regexp):
    return lambda s : not(bool(re.compile(regexp).search(s)))



class NumericRegexp:
    unsigned_int = '[0-9]+'
    int = '[+-]?'+unsigned_int

    unsigned_fraction = '\.'+unsigned_int
    fraction = '[+-]?'+unsigned_fraction
    float = '('+int+'('+unsigned_fraction+')?|'+int+'\.|'+fraction+')'

    nan = '[nN][aA][nN]'
    inf = '[+-]?[iI][nN][fF]'

    exp = float+'([eE]'+float+')?'
    num = '('+nan+'|'+inf+'|'+exp+')'


