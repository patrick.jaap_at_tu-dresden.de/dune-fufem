

class Iterate:
    @classmethod
    def toEnd(cls, it):
        try:
            while True:
                it.next()
        except StopIteration:
            pass

    @classmethod
    def until(cls, it, stop, withLast=True):
        uit = UntilIterator(it, stop, withLast)
        Iterate.toEnd(uit)




class BufferedIterator:

    def __init__(self, it, size=1):
        self.it = it
        self.size = size
        self.buffer = []
        self.pos = -1
        self.stoped = False

    def __iter__(self):
        return self

    def next(self):
        if self.stoped:
            raise StopIteration
        if self.pos == len(self.buffer)-1:
            try:
                next = self.it.next()
                self.buffer.append(next)
            except StopIteration:
                self.stoped = True
                raise StopIteration
        if len(self.buffer)>self.size:
            self.buffer.pop(0)
            self.pos -= 1
        self.pos += 1
        return self.buffer[self.pos]

    def back(self):
        if self.pos<0:
            raise NameError('You can only go back '+str(size)+' steps!')
        self.pos -= 1
        self.stoped = False
        return self.pos+1

    def end(self):
        try:
            self.next()
        except StopIteration:
            self.stoped = True
            return True
        self.back()
        return False



def FileIterator(file):
    l = file.readline()
    while (l!=''):
        yield l
        l = file.readline()



def UntilIterator(it, stop, withLast=True):
    while (True):
        item = it.next()
        if (stop(item)):
            if (withLast):
                yield item
            else:
                it.back()
            return
        yield item



def FilterIterator(it, include, mutate=(lambda x:x)):
    while (True):
        item = it.next()
        if (include(item)):
            yield mutate(item)



def HierarchyIterator(it, up, down, hideupdown=False):
    level = 0
    while (True):
        item = it.next()
        upItem = up(item)
        downItem = down(item)
        if (upItem):
            level += 1
        if (not hideupdown) or ((not upItem) and (not downItem)):
            yield (item, level)
        if (downItem):
            level -= 1
            if level < 0:
                level = 0

