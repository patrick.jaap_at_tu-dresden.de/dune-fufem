# Master (will become release 2.8)

- constructBoundaryDofs:
    - Small interface change in the template parameters: drop `blocksize` and replace it by `BitSetVector`
    - The method can now handle generic `dune-functions` basis types, as long as we have consistency in the data types

- assembleGlobalBasisTransferMatrix:
    - Support for all bases in `dune-functions` compatible form added

- `istlMatrixBackend` can now be used with `MultiTypeBlockMatrix`

- The class `DuneFunctionsLocalMassAssembler` has been renamed to `MassAssembler`,
  moved into the namespace `Dune::Fufem`, and to the file `massassembler.hh`.
  The old class is still there, but it is deprecated now.

## Deprecations and removals

- ...
