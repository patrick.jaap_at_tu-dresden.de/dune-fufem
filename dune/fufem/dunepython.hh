// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_PYTHON_HH
#define DUNE_FUFEM_PYTHON_HH

// Only introduce the dune-python interface if python
// was found and enabled.
#if HAVE_PYTHON || DOXYGEN

#include <dune/fufem/python/reference.hh>
#include <dune/fufem/python/module.hh>
#include <dune/fufem/python/common.hh>
#include <dune/fufem/python/conversion.hh>
#include <dune/fufem/python/function.hh>

#else
    #warning dunepython.hh was included but python was not found or enabled!
#endif // end of #if HAVE_PYTHON


#endif

