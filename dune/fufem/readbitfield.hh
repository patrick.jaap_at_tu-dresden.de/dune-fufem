#ifndef DUNE_FUFEM_READ_BITFIELD_HH
#define DUNE_FUFEM_READ_BITFIELD_HH

#include <dune/common/exceptions.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/fufem/boundarypatch.hh>

#ifdef HAVE_AMIRAMESH
#include <amiramesh/AmiraMesh.h>
#endif

//! Reads bit values from an external AmiraMesh file and writes them into a BitSetVector<1>
template <int ncomp>
inline void readBitField(Dune::BitSetVector<ncomp>& field, int size, const std::string& filename) {

    field.resize(size, false);
#ifdef HAVE_AMIRAMESH
    // /////////////////////////////////////////////////////
    // Load the AmiraMesh file
    std::unique_ptr<AmiraMesh> am(AmiraMesh::read(filename.c_str()));
    if (!am)
        DUNE_THROW(Dune::Exception, "Could not open AmiraMesh file: " << filename);
    // check for consistency
    if (size != am->nElements("Nodes"))
        DUNE_THROW(Dune::IOError, "Data in the file doesn't have the expected size!");

    float* am_values_float = NULL;
    double* am_values_double = NULL;

    if (am->findData("Nodes", HxFLOAT, 1, "Data") || am->findData("Nodes",HxDOUBLE, 1, "Data")) {
        // get the data field
        AmiraMesh::Data* am_ValueData = am->findData("Nodes", HxFLOAT, 1,"Data");
        if (am_ValueData) {
            am_values_float = (float*) am_ValueData->dataPtr();
            for (int i=0; i<am->nElements("Nodes"); i++)
                field[i] = (am_values_float[i] > 0.5);
        } else {
            am_ValueData = am->findData("Nodes", HxDOUBLE, 1, "Data");
            am_values_double = (double*) am_ValueData->dataPtr();
            for (int i=0; i<am->nElements("Nodes"); i++)
                field[i] = (am_values_double[i] > 0.5);
        }
    } else {
        // get the data field
        AmiraMesh::Data* am_ValueData = am->findData("Nodes", HxFLOAT, 1, "values");
        if (am_ValueData) {
            am_values_float = (float*) am_ValueData->dataPtr();
            for (int i=0; i<am->nElements("Nodes"); i++)
                field[i] = (am_values_float[i] > 0.5);
        } else {
            am_ValueData = am->findData("Nodes", HxDOUBLE, 1, "values");
            if (am_ValueData) {
                am_values_double = (double*) am_ValueData->dataPtr();
                for (int i=0; i<am->nElements("Nodes"); i++)
                    field[i] = (am_values_double[i] > 0.5);
            }
            else
                DUNE_THROW(Dune::IOError, "No data found in the file!");
        }
    }

    std::cout << "BitField " << filename << " loaded successfully!" << std::endl;
#else
    DUNE_THROW(Dune::Exception,"You need AmiraMesh to use readBitField");
#endif
}

//! Read level boundarypatch from an AmiraMesh file. */
template <class GridType>
DUNE_DEPRECATED_MSG("This method is deprecated. Please use the version with GridView as template.")
void readBoundaryPatch(BoundaryPatch<typename GridType::LevelGridView>& patch,
                       const std::string& filename) {
    /** \todo Not very memory efficient! */
    Dune::BitSetVector<1> vertices(patch.gridView().size(GridType::dimension),false);
    readBitField(vertices, vertices.size(), filename);
    patch.setup(patch.gridView(), vertices);
}

//! Read level boundarypatch from an AmiraMesh file. */
template <class GridView>
void readBoundaryPatch(BoundaryPatch<GridView>& patch,
                       const std::string& filename) {
    /** \todo Not very memory efficient! */
    Dune::BitSetVector<1> vertices(patch.gridView().size(GridView::dimension), false);
    readBitField(vertices, vertices.size(), filename);
    patch.setup(patch.gridView(), vertices);
}



#endif
