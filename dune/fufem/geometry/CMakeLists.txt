install(FILES
    convexpolyhedron.hh
    polyhedrondistance.hh
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/fufem/grid)
