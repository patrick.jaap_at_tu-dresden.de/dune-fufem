#ifndef DUNE_FUFEM_GEOMETRY_CONVEXPOLYHEDRON_HH
#define DUNE_FUFEM_GEOMETRY_CONVEXPOLYHEDRON_HH

#include <dune/matrix-vector/axpy.hh>

template <class Coordinate> struct ConvexPolyhedron {
  std::vector<Coordinate> vertices;

  template <class DynamicVector>
  Coordinate barycentricToGlobal(DynamicVector const &b) const {
    assert(b.size() == vertices.size());
    Coordinate r(0);
    for (size_t i = 0; i < b.size(); i++)
      Dune::MatrixVector::addProduct(r, b[i], vertices[i]);
    return r;
  }

  template <class DynamicVector>
  void sanityCheck(DynamicVector const &b, double tol) const {
    double sum = 0;
    for (size_t i = 0; i < b.size(); i++) {
      if (b[i] < -tol)
        DUNE_THROW(Dune::Exception, "No barycentric coords " << b[1] << " nr. "
                                                             << i);
      sum += b[i];
    }
    if (sum > 1 + tol)
      DUNE_THROW(Dune::Exception, "No barycentric coords, sum: " << sum);
  }
};
#endif
