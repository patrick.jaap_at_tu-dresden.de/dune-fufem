#ifndef BASIS_GRID_FUNCTION_HH
#define BASIS_GRID_FUNCTION_HH


#include <dune/common/typetraits.hh>
#include <dune/common/fvector.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/common/function.hh>

#include <dune/localfunctions/common/virtualinterface.hh>

#include <dune/istl/bvector.hh>

#include <dune/matrix-vector/axpy.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>


template<class B, class C> class BasisGridFunction;


#include <dune/fufem/refinedfehelper.hh>




template<class Basis, class Coefficients, class RangeType>
struct LocalEvaluator
{
    template<class Element, class LocalDomainType>
    static void evaluateLocal(const Basis& basis,
                              const Coefficients& coefficients,
                              const Element& e,
                              const LocalDomainType& x,
                              RangeType& y)
    {
        typedef typename Basis::LinearCombination LinearCombination;
        typedef typename Basis::LocalFiniteElement::Traits::LocalBasisType LB;
        typedef typename LB::Traits::RangeType LBRangeType;

        const LB& localBasis = basis.getLocalFiniteElement(e).localBasis();

        std::vector<LBRangeType> values;
        localBasis.evaluateFunction(x, values);

        y = 0;
        for (size_t i=0; i<localBasis.size(); ++i)
        {
            int index = basis.index(e, i);
            if (basis.isConstrained(index))
            {
                const LinearCombination& constraints = basis.constraints(index);
                for (size_t w=0; w<constraints.size(); ++w)
                    Dune::MatrixVector::addProduct(y, values[i] * constraints[w].factor, coefficients[constraints[w].index]);
            }
            else
                Dune::MatrixVector::addProduct(y, values[i], coefficients[index]);
        }
    }
};



template<class Basis, class Coefficients, class RangeType>
struct LocalDerivativeEvaluator
{
    template<class Element, class LocalDomainType, class DerivativeType>
    static void evaluateDerivativeLocal(const Basis& basis,
                                        const Coefficients& c,
                                        const Element& e,
                                        const LocalDomainType& x,
                                        DerivativeType& d)
    {
        DUNE_THROW(Dune::NotImplemented, "evaluateDerivativeLocal not implemented for this RangeType");
    }
};



template<class Basis, class Coefficients, class RangeFieldType, int m>
struct LocalDerivativeEvaluator<Basis, Coefficients, typename Dune::FieldVector<RangeFieldType,m> >
{
    //! Compute y+= a*A*B
    template<class Weight, class Jacobian, class DerivativeType>
    static void gaxpy(const Weight& a,
                      const typename Coefficients::value_type& A,
                      const Jacobian& B,
                      DerivativeType& y)
    {
        for (int i=0; i<DerivativeType::rows; ++i)
            for (int j=0; j<DerivativeType::cols; ++j)
                y[i][j] += a * A[i] * B[0][j];
    }

    //! Compute y+= A*B
    template<class Jacobian, class DerivativeType>
    static void gaxpy(const typename Coefficients::value_type& A,
                      const Jacobian& B,
                      DerivativeType& y)
    {
        for (int i=0; i<DerivativeType::rows; ++i)
            for (int j=0; j<DerivativeType::cols; ++j)
                y[i][j] += A[i] * B[0][j];
    }

    template<class Element, class LocalDomainType, class DerivativeType>
    static void evaluateDerivativeLocal(const Basis& basis,
                                        const Coefficients& coefficients,
                                        const Element& e,
                                        const LocalDomainType& x,
                                        DerivativeType& d)
    {
        typedef typename Basis::LinearCombination LinearCombination;
        typedef typename Basis::LocalFiniteElement::Traits::LocalBasisType LB;
        typedef typename LB::Traits::JacobianType LBJacobianType;

        const LB& localBasis = basis.getLocalFiniteElement(e).localBasis();

        std::vector<LBJacobianType> localJacobians;
        localBasis.evaluateJacobian(x, localJacobians);

        DerivativeType localD(0.0);
        for (size_t i=0; i<localBasis.size(); ++i)
        {
            int index = basis.index(e, i);
            if (basis.isConstrained(index))
            {
                const LinearCombination& constraints = basis.constraints(index);
                for (size_t w=0; w<constraints.size(); ++w)
                    gaxpy(constraints[w].factor, coefficients[constraints[w].index], localJacobians[i], localD);
            }
            else
                gaxpy(coefficients[index], localJacobians[i], localD);
        }

        typename Element::Geometry::JacobianInverseTransposed J = e.geometry().jacobianInverseTransposed(x);
        for (int j=0; j<DerivativeType::rows; ++j)
            J.mv(localD[j], d[j]);
    }
};



/**
 * \brief Base class for BasisGridFunction
 *
 * This class does not depend on the basis and allows to check
 * if a function is a BasisGridFunction using a dynamic_cast
 * without knowing the basis type.
 *
 * Furthermore it adds additional virtual functions
 * to provide information depending on the basis without
 * knowing its type.
 *
 * \tparam GridType Type of underlying grid
 */
template<class GridType>
class BasisGridFunctionInfo
{
        typedef typename GridType::template Codim<0>::Entity Element;

    public:

        /**
         * \brief Check if LocalFiniteElement associated with element is refined
         */
        virtual bool isRefinedLocalFiniteElement(const Element& e) const = 0;

        /**
         * \brief Get QuadratureRuleKey needed to integrate on given element
         */
        virtual QuadratureRuleKey quadratureRuleKey(const Element& e) const = 0;
};



/**
 * \brief Grid function obtained from global basis and coefficient vector
 *
 * \note An object of this class does not store the coefficient vector
 * \tparam B Global function space basis type
 * \tparam C Coefficient vector type
 */
template<class B, class C>
class BasisGridFunction :
    public VirtualGridViewFunction<typename B::GridView, typename C::value_type>,
    public BasisGridFunctionInfo<typename B::GridView::Grid>
{
    protected:
        typedef VirtualGridViewFunction<typename B::GridView, typename C::value_type> BaseType;

    public:

        typedef typename BaseType::LocalDomainType LocalDomainType;
        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;
        typedef typename BaseType::DerivativeType DerivativeType;
        typedef typename BaseType::Element Element;
        typedef typename BaseType::GridView GridView;
        typedef typename BaseType::Grid Grid;

        //! The type of function space basis
        typedef B Basis;

        //! The type of the coefficient vectors
        typedef C CoefficientVector;



        /**
         * \brief Setup grid function
         *
         * \param basis Global function space basis
         * \param coefficients Coefficient vector
         */
        BasisGridFunction(const Basis& basis, const CoefficientVector& coefficients) :
            BaseType(basis.getGridView()),
            basis_(basis),
            coefficients_(coefficients)
        {
          assert(basis_.size() == coefficients_.size());
        }



        /**
         * \copydoc VirtualGridFunction::evaluateLocal
         */
        virtual void evaluateLocal(const Element& e, const LocalDomainType& x, RangeType& y) const
        {
            LocalEvaluator<B, CoefficientVector, RangeType>::evaluateLocal(basis_, coefficients_, e, x, y);
        }



        /**
         * \copydoc VirtualGridFunction::evaluateDerivativeLocal
         */
        void evaluateDerivativeLocal(const Element& e, const LocalDomainType& x, DerivativeType& d) const
        {
            LocalDerivativeEvaluator<B, CoefficientVector, RangeType>::evaluateDerivativeLocal(basis_, coefficients_, e, x, d);
        }



        /**
         * \copydoc BasisGridFunctionInfo::isRefinedLocalFiniteElement
         */
        virtual bool isRefinedLocalFiniteElement(const Element& e) const
        {
            return IsRefinedLocalFiniteElement<typename Basis::LocalFiniteElement>::value(basis_.getLocalFiniteElement(e));
        }



        /**
         * \brief Get QuadratureRuleKey needed to integrate on given element
         */
        virtual QuadratureRuleKey quadratureRuleKey(const Element& e) const
        {
            return QuadratureRuleKey(basis_.getLocalFiniteElement(e));
        }



        /**
         * \brief Export basis
         */
        const Basis& basis() const
        {
            return basis_;
        }



        /**
         * \brief Export coefficient vector
         */
        const CoefficientVector& coefficientVector() const
        {
            return coefficients_;
        }



    protected:

        const Basis& basis_;
        const CoefficientVector& coefficients_;
};


namespace Functions
{

template<class B, class C>
BasisGridFunction<B, C> makeFunction(const B& basis, const C& coeff)
{
    return BasisGridFunction<B,C>(basis, coeff);
}

} // end namespace Functions



#endif

