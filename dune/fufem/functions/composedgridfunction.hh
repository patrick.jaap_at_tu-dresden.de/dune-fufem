// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef COMPOSED_GRID_FUNCTION_HH
#define COMPOSED_GRID_FUNCTION_HH

#include <dune/fufem/functions/virtualgridfunction.hh>

/** \brief  A class implementing the composition of a GridFunction and another Function. The resulting Function therefore is again a GridFunction.
 *
 *  The composed function \f$ f\circ g:A\rightarrow C \f$ is implemented where \f$ g:A\rightarrow B \f$ is a 
 *  GridFunction and \f$ f:B\rightarrow C \f$ a general function derived from VirtualFunction.
 *  \tparam  GridType type of the Grid
 *  \tparam  BT type of \f$ range(f)\f$ and \f$ domain(g)\f$
 *  \tparam  CT type of \f$ range(g)\f$
 */
template <typename GridType, typename BT, typename CT>
class ComposedGridFunction :
    public VirtualGridFunction<GridType, CT>
{

    typedef VirtualGridFunction<GridType, CT> BaseType;

public:
    typedef typename BaseType::LocalDomainType LocalDomainType;
    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType RangeType;
    typedef typename BaseType::DerivativeType DerivativeType;
    typedef typename BaseType::Grid Grid;

    typedef VirtualGridFunction<GridType,BT> InnerFunctionType;
    typedef Dune::VirtualFunction<BT,RangeType> OuterFunctionType;

    typedef typename BaseType::Element Element;

    /** \brief Constructor
     *
     *  \param f outer function for composition
     *  \param g inner (grid-)function for composition
     */
    ComposedGridFunction(const OuterFunctionType& f, const InnerFunctionType& g):
        BaseType(g.grid()),
        f_(f),
        g_(g)
    {}

    /** \brief composed evaluation of all components using evalall/evalalllocal of the composed functions
     *
     *  \param e Grid Element on which to evaluate locally
     *  \param x point in \f$ A \f$ at which to evaluate
     *  \param y vector in \f$ C \f$ to store the function value in
     */
    virtual void evaluateLocal(const Element& e, const DomainType& x,  RangeType& y) const
    {
        BT ytemp;

        g_.evaluateLocal(e,x,ytemp);
        f_.evaluate(ytemp,y);

        return;
    }


    /** \brief evaluation of derivative in local coordinates
     *
     *  \param e Evaluate in local coordinates with respect to this element.
     *  \param x point in local coordinates at which to evaluate the derivative
     *  \param d will contain the derivative at x after return
     */
    virtual void evaluateDerivativeLocal(const Element& e, const LocalDomainType& x, DerivativeType& d) const
    {
        using VDFunc = VirtualDifferentiableFunction<BT,RangeType>;
        const VDFunc* vdf = dynamic_cast<const VDFunc*>(&f_);
        if (vdf)
        {
            BT ytemp;
            g_.evaluateLocal(e,x,ytemp); // g(x)

            typename VDFunc::DerivativeType dfgx; // f'(g(x))
            typename InnerFunctionType::DerivativeType dgx;                            // g'(x)

            vdf->evaluateDerivative(ytemp,dfgx);

            g_.evaluateDerivativeLocal(e,x,dgx);

            //  d = dfgx*dgx;
            d = dfgx.rightmultiplyany(dgx);

            return;
        }
        else
            DUNE_THROW(Dune::Exception,"This composed function is not piecewise differentiable on elements because its outer function isn't.");

        return;
    }

    virtual bool isDefinedOn(const Element& e) const
    {
        return g_.isDefinedOn(e);
    }

    ~ComposedGridFunction(){}

private:
    const OuterFunctionType& f_;
    const InnerFunctionType& g_;
};

#endif

