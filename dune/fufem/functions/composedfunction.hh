// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef COMPOSED_FUNCTION_HH
#define COMPOSED_FUNCTION_HH

// Borrow internal helper from dune-localfunctions
// for transition of function interface
#include <dune/localfunctions/common/localinterpolation.hh>

/** \brief  A class implementing the composition of two functions.
 *
 *  The composed function \f$ f\circ g:A\rightarrow C \f$ where \f$ g:A\rightarrow B \f$ and \f$ f:B\rightarrow C \f$
 *  is implemented.
 *  \tparam  AT type of \f$ domain(g)\f$
 *  \tparam  BT type of \f$ range(g)\f$ and \f$ domain(f)\f$
 *  \tparam  CT type of \f$ range(f)\f$
 */
template <typename AT, typename BT, typename CT>
class ComposedFunction
{
public:
    using DomainType = AT;
    using RangeType = CT;

    using InnerFunctionType = std::function<BT(AT)>;
    using OuterFunctionType = std::function<CT(BT)>;

    /** \brief Constructor
     *
     *  \param f outer function for composition
     *  \param g inner function for composition
     */
    template<class F, class G>
    ComposedFunction(const F& f, const G& g) :
        f_(Dune::Impl::makeFunctionWithCallOperator<BT>(f)),
        g_(Dune::Impl::makeFunctionWithCallOperator<AT>(g))
    {}

    /** \brief composed evaluation of one component using eval of the composed functions
     *
     *  \param x point in \f$ A \f$ at which to evaluate
     *  \param y vector in \f$ C \f$ to store the function value in
     */
    RangeType operator()(const DomainType& x)
    {
        return f_(g_(x));
    }

    /** \brief composed evaluation of one component using eval of the composed functions
     *
     * \deprecated Use operator() instead
     *
     *  \param x point in \f$ A \f$ at which to evaluate
     *  \param y vector in \f$ C \f$ to store the function value in
     */
    void evaluate(const DomainType& x,  RangeType& y) const
    {
        y = f_(g_(x));
    }

    ~ComposedFunction(){}

private:
    OuterFunctionType f_;
    InnerFunctionType g_;

};

#endif

