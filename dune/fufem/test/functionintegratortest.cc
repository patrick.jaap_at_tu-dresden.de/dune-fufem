#include <config.h>

#include <array>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <list>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/function.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <dune/fufem/functiontools/functionintegrator.hh>

#include "common.hh"

/** \brief Polynomial on arbitrary dimensional Domain; Max degree and number of terms is bounded for the purposes here. Multiindices for exponents may repeat.
 *
 */
template <class DT>
class TestPolynomial:
    public Dune::VirtualFunction<DT,Dune::FieldVector<double, 1> >
{
        typedef Dune::VirtualFunction<DT,Dune::FieldVector<double, 1> > BaseType;

    public:
        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;

        enum {DomainDim = DomainType::dimension,
              RangeDim  = RangeType::dimension};

        TestPolynomial():
            degree(0),
            integralOnUnitCube(0.0)
        {
            int maxdegree = (rand() % 5) + 2;
            int nCoeffs = (rand() % 3) + 3;

//            std::cout << "maxdegree= " << maxdegree << " " << "nCoeffs = " << nCoeffs << std::endl;

            for (int i=0; i<nCoeffs; ++i)
            {
                PolyCoeff<DomainDim> new_coeff;
                new_coeff.coeff = (10.0*rand())/RAND_MAX;
//                std::cout << "i: " << i << " " << "a = " << new_coeff.coeff << ", MI=( ";
                int indexsum = 0;
                double product = 1;
                for (int d=0;d<DomainDim;++d)
                {
                    new_coeff.multiindex[d] = std::min((rand()%maxdegree),maxdegree-indexsum);
//                    std::cout << new_coeff.multiindex[d] << " ";
                    indexsum += new_coeff.multiindex[d];
                    product *= new_coeff.multiindex[d] + 1;
                }
                coeffs.push_back(new_coeff);
//                std::cout << ")" << std::endl;

                assert(indexsum <= maxdegree);

                degree = std::max(indexsum, degree);

                integralOnUnitCube += new_coeff.coeff / product;
            }

//            std::cout << "degree: " << degree << std::endl;
//            std::cout << "integral on unit cube: " << integralOnUnitCube << std::endl;

        }

        virtual void evaluate(const DomainType& x, RangeType& y) const
        {
            y = 0.0;

            typename CoeffsType::const_iterator coit=coeffs.begin();
            typename CoeffsType::const_iterator coend=coeffs.end();

            for (; coit!=coend; ++coit)
            {
                double temp = coit->coeff;
                for (int d=0; d<DomainDim; ++d)
                    temp *= pow(x[d],coit->multiindex[d]);
                y += temp;
            }

            return;
        }

        int degree;

        double integralOnUnitCube;

    private:
        template <int dim>
        struct PolyCoeff
        {
            std::array<int,dim> multiindex;
            double coeff;

            PolyCoeff():
                coeff(0.0)
            {
                multiindex.fill(0.0);
            }
        };

        typedef std::list<PolyCoeff<DomainDim> > CoeffsType;
        CoeffsType coeffs;
};

/** \brief TestSuite for FunctionIntegrator
 *
 *  Test the FunctionIntegrator by integrating a TestPolynomial over the unit(hyper)cube and comparing to the known value
 */
struct FunctionIntegratorTestSuite
{
    template <class GridType>
    bool check(GridType& grid)
    {
        typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate DomainType;

        typedef TestPolynomial<DomainType> FunctionType;

        int n_testcases = 5;


        for (int run=0; run<n_testcases; ++run)
        {
            FunctionType p;

            double I = FunctionIntegrator::integrate(grid.leafGridView(),p,p.degree);
            if (std::abs(I-p.integralOnUnitCube)>1e-12)
            {
                std::cout << "I = " << I << ", integralOnUnitCube= " << p.integralOnUnitCube << std::endl;
                std::cout << "I - integralOnUnitCube= " << I-p.integralOnUnitCube << std::endl;

                return false;
            }
        }

        return true;
    }
};

template<class GridType>
auto constructCoarseUnitSquareGrid()
{
    const int dim=2;
    typename Dune::GridFactory<GridType> gridFactory;

    typename Dune::FieldVector<double,dim> pos(0);
    gridFactory.insertVertex(pos);
    for (int i=0; i<dim; i++) {
        pos = 0;   pos[i] = 1;
        gridFactory.insertVertex(pos);
    }
    pos = 1.0;
    gridFactory.insertVertex(pos);


    std::vector<unsigned int> element(dim+1);
    for (int i=0; i<dim+1; i++)
    {
        element[i] = i;
    }
    std::cout << std::endl;

    gridFactory.insertElement( Dune::GeometryTypes::simplex(dim), element);

    for (int i=dim; i>=0; i--)
    {
        element[i] = i+1;
    }
    std::cout << std::endl;

    gridFactory.insertElement( Dune::GeometryTypes::simplex(dim), element);

    return gridFactory.createGrid();
}

template<class GridType, class TestSuite>
bool checkWithAdaptiveSquareGrid(TestSuite& suite, int uniformRefinements, int localRefinements, std::string name)
{
    auto gridPtr = std::unique_ptr<GridType>(constructCoarseUnitSquareGrid<GridType>());
    GridType& grid = *gridPtr;

    grid.globalRefine(uniformRefinements);
    refineLocally(grid, localRefinements);

    std::cout << "Running test on " << name << " with " << grid.size(0) << " elements." << std::endl;

    bool passed = suite.check(grid);
    if (passed)
        std::cout << "All tests passed with " << name << "." << std::endl;
    return passed;
}

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    std::cout << "This is the FunctionIntegratorTest" << std::endl;

    bool passed = true;

    FunctionIntegratorTestSuite tests;
    passed = checkWithStandardStructuredGrids(tests);


#if HAVE_UG
    passed = passed and checkWithAdaptiveSquareGrid<Dune::UGGrid<2> >(tests, 3, 3, "UGGrid<2>");
#endif

#if HAVE_DUNE_ALUGRID
    passed = passed and checkWithAdaptiveSquareGrid<Dune::ALUGrid<2, 2, Dune::simplex, Dune::nonconforming> >(tests, 3, 3, "ALUGrid<2,2,simplex,nonconforming>");
#endif


    return passed ? 0 : 1;

}
