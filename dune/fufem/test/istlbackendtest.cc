#include <config.h>
#include <dune/common/test/testsuite.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/indices.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/multitypeblockmatrix.hh>
#include <dune/istl/multitypeblockvector.hh>

#include <dune/fufem/assemblers/istlbackend.hh>
#include <dune/fufem/test/common.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

// Due to a formerly unnoticed bug, activate bounds checking:
#define DUNE_CHECK_BOUNDS 1

using namespace Dune;

template<class Entry, class Matrix, class RowIndex, class ColIndex>
TestSuite testISTLBackend(Matrix& mat, RowIndex&& row, ColIndex&& col) {
  TestSuite suite;

  auto backend = Fufem::istlMatrixBackend<Entry>(mat);

  suite.check(backend(row, col) == 1.0, "Check matrix access") << "Type" << typeid(mat).name() << " failed";

  return suite;
}

template<class Matrix>
TestSuite testMultiType(Matrix& mat) {
  TestSuite suite;

  auto backend = Fufem::istlMatrixBackend(mat);
  auto pattern = backend.patternBuilder();

  // create small grid
  auto* grid = constructCoarseYaspGrid<3>();
  auto gridView = grid->leafGridView();

  // create small basis
  namespace BB = Functions::BasisBuilder;

  // create a composite basis matching the matrix type
  const auto basis = BB::makeBasis(
                        gridView,
                        composite(
                          BB::power<3>(
                            BB::lagrange<1>()
                          ),
                          BB::power<5>(
                            BB::lagrange<0>()
                          )
                        )
                      );

  pattern.resize(basis,basis);

  // set the active entries
  auto localView = basis.localView();
  for(const auto& e : elements(gridView))
  {
    localView.bind(e);
    for(size_t i=0; i<localView.size(); i++)
    {
      auto idx = localView.index(i);
      pattern.insertEntry(idx,idx);
    }
  }

  pattern.setupMatrix();
  mat = 1.0;

  for(const auto& e : elements(gridView))
  {
    localView.bind(e);
    for(size_t i=0; i<localView.size(); i++)
    {
      auto idx = localView.index(i);
      suite.check(backend(idx, idx) == 1.0,
                  "Check matrix access") << "Type" << typeid(mat).name() << " failed";
    }
  }

  return suite;
}

int main(int argc, char** argv) {
  MPIHelper::instance(argc, argv);

  TestSuite suite;

  using K = double;

  // Matrix<FieldMatrix<K, 1,1>
  {
    using Matrix = Matrix<FieldMatrix<K,1,1>>;
    Matrix mat(2,2);
    mat =1.0;

    // check with multiindex with length 1
    {
      auto idx = std::array<size_t, 1>{{1}};

      suite.subTest(testISTLBackend<K>(mat, idx, idx));

      // check const:
      const auto& const_mat = mat;
      suite.subTest(testISTLBackend<const K>(const_mat, idx, idx));
    }
    // check with multiindex with length 2
    {
      // someone might set a trailing zero to account for the matrix
      // character of FieldMatrix<K, 1,1>:
      auto idx = std::array<size_t, 2>{{1, 0}};

      suite.subTest(testISTLBackend<K>(mat, idx, idx));

      // check const:
      const auto& const_mat = mat;
      suite.subTest(testISTLBackend<const K>(const_mat, idx, idx));
    }
  }

  // Matrix<FieldMatrix<K, n,n> n>1
  {
    constexpr int n = 2;
    using Matrix = Matrix<FieldMatrix<K, n, n>>;
    Matrix mat(2,2);
    mat = 1.0;

    auto idx = std::array<size_t, 2>{{1, 1}};

    suite.subTest(testISTLBackend<K>(mat, idx, idx));

    // check const:
    const auto& const_mat = mat;
    suite.subTest(testISTLBackend<const K>(const_mat, idx, idx));
  }

  // MultiTypeMatrix
  {
    using Matrix = MultiTypeBlockMatrix<MultiTypeBlockVector<BCRSMatrix<FieldMatrix<double,3,3>>,BCRSMatrix<FieldMatrix<double,3,5>>>,
                                        MultiTypeBlockVector<BCRSMatrix<FieldMatrix<double,5,3>>,BCRSMatrix<FieldMatrix<double,5,5>>>>;

    Matrix mat;

    suite.subTest(testMultiType(mat));
  }

  return suite.exit();
}
