// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#include <config.h>

#include <array>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/common/fmatrix.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/istl/io.hh>

#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/istlbackend.hh>

#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/hierarchicvectorwrapper.hh>



int main (int argc, char *argv[])
{
  Dune::MPIHelper::instance(argc, argv);

//  const int dim = 2;
//  const int refine = 9;

//  const int dim = 2;
//  const int refine = 4;

  const int dim = 2;
  const int refine = 1;

  // Build a test grid
  typedef Dune::YaspGrid<dim> GridType;

  Dune::FieldVector<double,dim> h(1);
  std::array<int,dim> n;
  n.fill(2);
  n[0] = 3;

  GridType grid(h,n);
  grid.globalRefine(refine);

  using namespace Dune::Functions::BasisBuilder;

  static const std::size_t K = 2;
//  static const std::size_t N = 4;
  static const std::size_t N = 1;

  // Construct a function space basis for the grid
//  auto basis = makeBasis(
//    grid.leafGridView(),
//    power<N>(
//      lagrange<K>(),
//      leafBlockedInterleaved()
//    ));

  auto basis = makeBasis(
    grid.leafGridView(),
      lagrange<K>()
    );

  using Field = double;
  using Basis = decltype(basis);
  using Vector = Dune::BlockVector<Dune::FieldVector<Field,N>>;
  //using VectorBackend = typename Dune::Functions::HierarchicVectorWrapper<Vector, Field>;

  using LocalMatrix =  Dune::FieldMatrix<Field,N,N>;
  using Matrix = Dune::BCRSMatrix<LocalMatrix>;

  using Assembler = Dune::Fufem::DuneFunctionsOperatorAssembler<Basis, Basis>;

  auto matrix = Matrix{};

  auto matrixBackend = Dune::Fufem::istlMatrixBackend(matrix);
  auto patternBuilder = matrixBackend.patternBuilder();

  auto assembler = Assembler{basis, basis};

  assembler.assembleBulkPattern(patternBuilder);

  patternBuilder.setupMatrix();

  using FiniteElement = std::decay_t<decltype(basis.localView().tree().finiteElement())>;

  auto vintageBulkAssembler = LaplaceAssembler<GridType,FiniteElement, FiniteElement>();

  auto localAssembler = [&](const auto& element, auto& localMatrix, auto&& trialLocalView, auto&& ansatzLocalView){
    vintageBulkAssembler.assemble(element, localMatrix, trialLocalView.tree().finiteElement(), ansatzLocalView.tree().finiteElement());
  };

  assembler.assembleBulkEntries(matrixBackend, localAssembler);

  auto one = Vector{};
  auto zero = Vector{};

  auto  oneBE = Dune::Fufem::istlVectorBackend(one);
  auto  zeroBE = Dune::Fufem::istlVectorBackend(zero);

  oneBE.resize(basis);
  zeroBE.resize(basis);

//  VectorBackend(one).resize(basis);
//  VectorBackend(zero).resize(basis);

  one = 1;
  
  matrix.mv(one,zero);

  std::cout << zero.infinity_norm() << std::endl;

  return 0;
}

