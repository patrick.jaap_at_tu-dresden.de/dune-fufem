#include <config.h>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>
#include <dune/fufem/functiontools/gridfunctionadaptor.hh>
#include <dune/fufem/functionspacebases/conformingbasis.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include "common.hh"


template <int dim, class RT>
class SinSin :
    public Dune::VirtualFunction<typename Dune::FieldVector<double, dim>, RT>
{
    public:
        void evaluate(const Dune::FieldVector<double, dim>& x, RT& y) const
        {
            double r=1.0;
            for(int i=0; i<dim; ++i)
                r*=sin(x[i]);
            y = r;
        }
};



template<class RT>
struct Suite
{
    template<class GridType>
    bool check(GridType& grid)
    {
        const int dim = GridType::dimensionworld;
        typedef typename GridType::LeafGridView View;
        typedef DuneFunctionsBasis<Dune::Functions::LagrangeBasis<View, 1> > NonconformingBasis;
        typedef ConformingBasis<NonconformingBasis> Basis;

        typedef Dune::BlockVector<RT> Vector;

        NonconformingBasis nonconformingBasis(grid.leafGridView());
        Basis basis(nonconformingBasis);

        Vector v;

        SinSin<dim, RT> f;
        Functions::interpolate(basis, v, f);

        GridFunctionAdaptor<Basis> adaptor(basis, true);

        refineLocally(grid, 1);

        nonconformingBasis.update(grid.leafGridView());
        basis.update(grid.leafGridView());

        adaptor.adapt(v);

        bool passed = (v.size() == basis.size());

        return passed;
    }

};

template<class RT>
bool checkForRangeType()
{
    Suite<RT> tests;
    return checkWithStandardAdaptiveGrids(tests);
}

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    bool passed = true;

    passed = passed and checkForRangeType<Dune::FieldVector<double, 1> >();
    passed = passed and checkForRangeType<Dune::FieldVector<double, 2> >();
    passed = passed and checkForRangeType<Dune::FieldVector<double, 3> >();

    passed = passed and checkForRangeType<Dune::FieldVector<int, 1> >();
    passed = passed and checkForRangeType<Dune::FieldVector<int, 2> >();
    passed = passed and checkForRangeType<Dune::FieldVector<int, 3> >();

    passed = passed and checkForRangeType<Dune::FieldMatrix<double, 2, 2> >();
    passed = passed and checkForRangeType<Dune::FieldMatrix<double, 2, 3> >();
    passed = passed and checkForRangeType<Dune::FieldMatrix<double, 3, 2> >();

    passed = passed and checkForRangeType<Dune::FieldMatrix<int, 2, 2> >();
    passed = passed and checkForRangeType<Dune::FieldMatrix<int, 2, 3> >();
    passed = passed and checkForRangeType<Dune::FieldMatrix<int, 3, 2> >();

    return passed ? 0 : 1;
}
