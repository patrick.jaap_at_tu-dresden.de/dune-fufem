#ifndef REDUCED_CONFORMING_BASIS_HH
#define REDUCED_CONFORMING_BASIS_HH

#include "dune/fufem/functionspacebases/functionspacebasis.hh"
#include "dune/fufem/functionspacebases/dofconstraints.hh"
#include "dune/fufem/functionspacebases/conformingbasis.hh"


/** \brief Generic class for conforming global basis
 *
 * This class provides a generic global basis of a conforming (C^0)
 * finite element space. The space is constructed from a basis of
 * a nonconforming space using an internal DOFConstraints. You can
 * also provide the latter on your own using the appropiate constructor.
 *
 * The size of this basis only incorporates the unconstrained DOF.
 * Notice that the index method returns values larger then the size
 * for constrained DOFs not appearing in the basis.
 *
 * \tparam NonconformingBasis The underlying nonconforming basis.
 */
template <class NonconformingBasis>
class ReducedConformingBasis :
    public ConformingBasis<NonconformingBasis>

{
    protected:
        typedef ConformingBasis<NonconformingBasis> Base;
        typedef typename Base::Element Element;


    public:
        typedef typename Base::GridView GridView;
        typedef typename Base::ReturnType ReturnType;
        typedef typename Base::LocalFiniteElement LocalFiniteElement;
        typedef typename Base::BitVector BitVector;
        typedef typename Base::LinearCombination LinearCombination;

        /**
         * \brief Setup conforming global basis
         *
         * This constructor will take some time since the interpolation values
         * for the conforming basis need to be build.
         *
         * \param ncBasis Global basis of underlying nonconforming function space
         */
        ReducedConformingBasis(const NonconformingBasis& ncBasis) :
            Base(ncBasis)
        {
            reorder();
        }

        /**
         * \brief Setup conforming global basis
         *
         * Using this constructor you can provide a custom DOFConstrains object.
         * This needs to be set up before using the basis and after grid modification manually.
         *
         * \param ncBasis Global basis of underlying nonconforming function space
         * \param dofConstraints Set of interpolation values for the conforming subspace basis
         */
        ReducedConformingBasis(const NonconformingBasis& ncBasis, const DOFConstraints& dofConstraints) :
            Base(ncBasis, dofConstraints)
        {
            reorder();
        }

        virtual ~ReducedConformingBasis()
        {}

        void update(const GridView& gridview)
        {
            Base::update(gridview);
            reorder();
        }

        size_t size() const
        {
            return unconstrainedSize_;
        }

        int index(const Element& e, const int i) const
        {
            return reorderedIndex_[ncBasis_.index(e, i)];
        }

        using Base::getLocalFiniteElement;
        using Base::isConstrained;
        using Base::constraints;


    protected:

        using Base::dofConstraints_;
        using Base::ncBasis_;

        //! Reorder DOFs such that unconstrained appear first.
        void reorder()
        {
            typedef typename DOFConstraints::Interpolation Interpolation;

            BitVector& isConstrained = const_cast<DOFConstraints*>(dofConstraints_)->isConstrained();
            Interpolation& interpolation = const_cast<DOFConstraints*>(dofConstraints_)->interpolation();

            reorderedIndex_.resize(ncBasis_.size());

            int lastUnconstrained = -1;
            int firstConstrained = ncBasis_.size();

            while (isConstrained[firstConstrained-1][0] and firstConstrained>0)
            {
                --firstConstrained;
                reorderedIndex_[firstConstrained] = firstConstrained;
            }

            while (lastUnconstrained<firstConstrained-1)
            {
                if (isConstrained[lastUnconstrained+1][0])
                {
                    ++lastUnconstrained;
                    --firstConstrained;

                    reorderedIndex_[lastUnconstrained] = firstConstrained;
                    reorderedIndex_[firstConstrained] = lastUnconstrained;

                    isConstrained[firstConstrained][0] = true;
                    interpolation[firstConstrained] = interpolation[lastUnconstrained];

                    isConstrained[lastUnconstrained][0] = false;
                    interpolation[lastUnconstrained].resize(0);

                    while (isConstrained[firstConstrained-1][0] and firstConstrained>0)
                    {
                        --firstConstrained;
                        reorderedIndex_[firstConstrained] = firstConstrained;
                    }
                }
                else
                {
                    ++lastUnconstrained;
                    reorderedIndex_[lastUnconstrained] = lastUnconstrained;
                }
            }
            for (size_t i=firstConstrained; i<ncBasis_.size(); ++i)
            {
                for (size_t j=0; j<interpolation[i].size(); ++j)
                    interpolation[i][j].index = reorderedIndex_[interpolation[i][j].index];
            }
            unconstrainedSize_ = firstConstrained;
        }

        int unconstrainedSize_;
        std::vector<int> reorderedIndex_;
};

#endif

