#ifndef P0_BASIS_HH
#define P0_BASIS_HH

/*
#warning This file is deprecated.  All implementations of function space bases in dune-fufem \
  are in the process of being replaced by counterparts in the new dune-functions module. \
  Those are syntactically different, but semantically very close to the dune-fufem implementations. \
  To get rid of this warning, replace all occurrences of the P0Basis<...> class in your code \
  by DuneFunctionsBasis<Dune::Functions::LagrangeBasis<...,0> >.
*/

/**
   @file
   @brief The canonical global basis for the space of piecewise constant finite element functions
 */

#include <dune/common/version.hh>
#include <dune/geometry/type.hh>
#include <dune/grid/common/mcmgmapper.hh>
#include <dune/localfunctions/lagrange/pqkfactory.hh>

#include <dune/fufem/functionspacebases/functionspacebasis.hh>


/** \brief The canonical global basis for the space of piecewise constant finite element functions
 *
 * \tparam GV GridView that the basis is defined for
 * \tparam RT The type for basis function values
 */
template <class GV, class RT=double>
class P0Basis :
    public FunctionSpaceBasis<
        GV,
        RT,
        typename Dune::PQkLocalFiniteElementCache<typename GV::Grid::ctype, RT, GV::dimension, 0>::FiniteElementType >
{
    protected:
        typedef typename GV::Grid::ctype ctype;

        typedef typename Dune::PQkLocalFiniteElementCache<typename GV::Grid::ctype, RT, GV::dimension, 0> FiniteElementCache;
        typedef typename FiniteElementCache::FiniteElementType LFE;

        typedef FunctionSpaceBasis<GV, RT, LFE> Base;
        typedef typename Base::Element Element;

        using Base::dim;
        using Base::gridview_;

        typedef typename Dune::MultipleCodimMultipleGeomTypeMapper<GV> P0BasisMapper;

    public:
        typedef typename Base::GridView GridView;
        typedef typename Base::ReturnType ReturnType;
        typedef typename Base::LocalFiniteElement LocalFiniteElement;
        typedef typename Base::LinearCombination LinearCombination;

        P0Basis(const GridView& gridview) :
            Base(gridview),
            mapper_(gridview, Dune::mcmgElementLayout())
        {}

        size_t size() const
        {
            return mapper_.size();
        }

        void update(const GridView& gridview)
        {
            Base::update(gridview);
            mapper_.update();
        }

        const LocalFiniteElement& getLocalFiniteElement(const Element& e) const
        {
            return cache_.get(e.type());
        }

        int index(const Element& e, const int i) const
        {
            return mapper_.index(e);
        }

    protected:
        FiniteElementCache cache_;
        P0BasisMapper mapper_;
};

#endif

