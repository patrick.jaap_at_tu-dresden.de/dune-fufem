﻿// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_SCALEDSUMOPERATORASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_SCALEDSUMOPERATORASSEMBLER_HH

#include <dune/common/fmatrix.hh>
#include <dune/fufem/assemblers/localoperatorassembler.hh>

/** \brief Local operator that is the sum of scaled local operator assemblers.
 *  All template parameters according to \class LocalOperatorAssembler
 */
template <class Grid, class TrialLocalFE, class AnsatzLocalFE,
          typename T = Dune::FieldMatrix<double, 1, 1>>
class ScaledSumOperatorAssembler
    : public LocalOperatorAssembler<Grid, TrialLocalFE, AnsatzLocalFE, T> {
public:
  using Base = LocalOperatorAssembler<Grid, TrialLocalFE, AnsatzLocalFE, T>;
  using Assembler = Base;
  using typename Base::Element;
  using typename Base::BoolMatrix;
  using typename Base::LocalMatrix;

  template <class AssemblerType>
  typename std::enable_if<
      std::is_base_of<Assembler, AssemblerType>::value>::type
  registerAssembler(double scale, const AssemblerType& assembler) {
    if (scale == 0.0)
      return;
    scaledAssemblers_.emplace_back(scale, &assembler);
  }

  template <class AssemblerType>
  typename std::enable_if<
      std::is_base_of<Assembler, AssemblerType>::value>::type
  registerAssembler(double scale, AssemblerType&& assembler) {
    if (scale == 0.0)
      return;
    heldAssemblers_.emplace_back(
        std::make_shared<AssemblerType>(std::move(assembler)));
    scaledAssemblers_.emplace_back(scale, heldAssemblers_.back().get());
  }

  template <class AssemblerType, class... Args>
  void registerAssembler(double scale, Args&&... args) {
    if (scale == 0.0)
      return;
    heldAssemblers_.emplace_back(
        std::make_shared<AssemblerType>(std::forward<Args>(args)...));
    scaledAssemblers_.emplace_back(scale, heldAssemblers_.back().get());
  }

public:
  virtual void indices(const Element& element, BoolMatrix& isNonZero,
                       const TrialLocalFE& tFE,
                       const AnsatzLocalFE& aFE) const {
    for (auto&& scaledAssembler : scaledAssemblers_) {
      BoolMatrix isNonZeroTmp(isNonZero.N(), isNonZero.M());
      scaledAssembler.second->indices(element, isNonZeroTmp, tFE, aFE);
      isNonZero += isNonZeroTmp;
    }
  }

  virtual void assemble(const Element& element, LocalMatrix& localMatrix,
                        const TrialLocalFE& tFE,
                        const AnsatzLocalFE& aFE) const {
    localMatrix = 0.0;
    for (auto&& scaledAssembler : scaledAssemblers_) {
      LocalMatrix m(localMatrix.N(), localMatrix.M());
      m = 0.0;
      scaledAssembler.second->assemble(element, m, tFE, aFE);
      m *= scaledAssembler.first;
      localMatrix += m;
    }
  }

  ScaledSumOperatorAssembler() {}

  ScaledSumOperatorAssembler(ScaledSumOperatorAssembler&& other)
      : heldAssemblers_(std::move(other.heldAssemblers_))
      , scaledAssemblers_(std::move(other.scaledAssemblers_)) {}

private:
  std::vector<std::shared_ptr<Assembler>> heldAssemblers_;
  std::vector<std::pair<double, const Assembler*>> scaledAssemblers_;

  // disable implicit copy
  ScaledSumOperatorAssembler(const ScaledSumOperatorAssembler&) = delete;
};

#endif // DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_SCALEDSUMOPERATORASSEMBLER_HH
