#ifndef CONVOLUTION_ASSEMBLER_HH
#define CONVOLUTION_ASSEMBLER_HH

#include <utility>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

// Borrow internal helper from dune-localfunctions
// for transition of function interface
#include <dune/localfunctions/common/localinterpolation.hh>

#include <dune/matrix-vector/addtodiagonal.hh>

#include "dune/fufem/staticmatrixtools.hh"
#include "dune/fufem/quadraturerules/quadraturerulecache.hh"



//** \brief Local mass assembler **//
template <class TrialBasis, class AnsatzBasis, class T=Dune::FieldMatrix<double,1,1> >
class ConvolutionAssembler
{
    public:
        typedef typename TrialBasis::LocalFiniteElement TrialLocalFE;
        typedef typename AnsatzBasis::LocalFiniteElement AnsatzLocalFE;
        typedef typename TrialBasis::GridView TrialGridView;
        typedef typename AnsatzBasis::GridView AnsatzGridView;

        typedef typename AnsatzGridView::template Codim<0>::Entity AnsatzElement;
        typedef typename TrialGridView::template Codim<0>::Entity TrialElement;
        typedef typename AnsatzElement::Geometry AnsatzGeometry;
        typedef typename TrialElement::Geometry TrialGeometry;
        typedef typename Dune::Matrix<T> LocalMatrix;

        typedef typename Dune::template FieldVector<double,TrialGridView::dimensionworld> TrialGlobalCoordinate;
        typedef typename Dune::template FieldVector<double,AnsatzGridView::dimensionworld> AnsatzGlobalCoordinate;
        typedef typename std::template pair<TrialGlobalCoordinate, AnsatzGlobalCoordinate> KernelArgument;

        typedef typename std::function<double(KernelArgument)> KernelFunction;

        template<class KFunc>
        ConvolutionAssembler(const KFunc& kernel, int tQuadOrder=2, int aQuadOrder=2):
            kernel_(Dune::Impl::makeFunctionWithCallOperator<KernelArgument>(kernel)),
            tQuadOrder_(tQuadOrder),
            aQuadOrder_(aQuadOrder)
        {}


        void assemble(const TrialElement& tElement, const TrialLocalFE& tFE, const AnsatzElement& aElement, const AnsatzLocalFE& aFE, LocalMatrix& localMatrix) const
        {
            static const int tDim = TrialGridView::dimension;
            static const int aDim = AnsatzGridView::dimension;

            typedef typename Dune::template FieldVector<double,tDim> TrialLocalCoordinate;
            typedef typename Dune::template FieldVector<double,aDim> AnsatzLocalCoordinate;

            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType TrialRangeType;
            typedef typename AnsatzLocalFE::Traits::LocalBasisType::Traits::RangeType AnsatzRangeType;

            // Make sure we got suitable shape functions
            assert(tFE.type() == tElement.type());
            assert(aFE.type() == aElement.type());

            // get geometries and store them
            const TrialGeometry tGeometry = tElement.geometry();
            const AnsatzGeometry aGeometry = aElement.geometry();

            localMatrix = 0.0;

            // get quadrature rule
            const Dune::template QuadratureRule<double, tDim>& tQuad
                = QuadratureRuleCache<double, tDim>::rule(tElement.type(), tQuadOrder_, IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE) );
            const Dune::template QuadratureRule<double, aDim>& aQuad
                = QuadratureRuleCache<double, aDim>::rule(aElement.type(), aQuadOrder_, IsRefinedLocalFiniteElement<AnsatzLocalFE>::value(aFE) );

            // store values of shape functions
            std::vector<TrialRangeType> tValues(tFE.localBasis().size());
            std::vector<AnsatzRangeType> aValues(aFE.localBasis().size());

            // store value of kernel
            double kernelValue;

            // loop over trial quadrature points
            for (size_t tPt=0; tPt < tQuad.size(); ++tPt)
            {
                // get quadrature point
                const TrialLocalCoordinate& tQuadPos = tQuad[tPt].position();

                // get global coordinates of quadrature point
                const TrialGlobalCoordinate tGlobalQuadPos = tGeometry.global(tQuadPos);

                // evaluate basis functions
                tFE.localBasis().evaluateFunction(tQuadPos, tValues);

                // compute overall integration weight from quadrature weight and integration element
                double tIntegrationWeight = tQuad[tPt].weight() * tGeometry.integrationElement(tQuadPos);

                // loop over ansatz quadrature points
                for (size_t aPt=0; aPt < aQuad.size(); ++aPt)
                {
                    // get quadrature point
                    const AnsatzLocalCoordinate& aQuadPos = aQuad[aPt].position();

                    // get global coordinates of quadrature point
                    const AnsatzGlobalCoordinate aGlobalQuadPos = aGeometry.global(aQuadPos);

                    // evaluate basis functions
                    aFE.localBasis().evaluateFunction(aQuadPos, aValues);

                    // compute overall integration weight from quadrature weight and integration element
                    double aIntegrationWeight = aQuad[aPt].weight() * aGeometry.integrationElement(aQuadPos);


                    // evauluate integration kernel at quadrature points
                    kernelValue = kernel_(std::make_pair(tGlobalQuadPos, aGlobalQuadPos));

                    for (size_t i=0; i<tFE.localBasis().size(); ++i)
                    {
                        double tIntegral = tValues[i]*tIntegrationWeight;
                        for (size_t j=0; j<aFE.localBasis().size(); ++j)
                        {
                            double aIntegral = aValues[i]*aIntegrationWeight;

                            Dune::MatrixVector::addToDiagonal(localMatrix[i][j], tIntegral * aIntegral * kernelValue);
                        } // end of ansatz functions loop
                    } // end of trial functions loop

                } // end of ansatz quadrature point loop
            } // end of trial quadrature point loop
        }


    protected:
        KernelFunction kernel_;
        const int tQuadOrder_;
        const int aQuadOrder_;
};


#endif

