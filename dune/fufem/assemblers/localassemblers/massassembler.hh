#ifndef MASS_ASSEMBLER_HH
#define MASS_ASSEMBLER_HH

#include <vector>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

#include <dune/typetree/visitor.hh>
#include <dune/typetree/traversal.hh>

#include <dune/matrix-vector/addtodiagonal.hh>

#include "dune/fufem/assemblers/localoperatorassembler.hh"
#include "dune/fufem/quadraturerules/quadraturerulecache.hh"


namespace Dune::Fufem
{

  namespace Impl
  {

    /** \brief Computes the inner product of the local basis functions
     *
     * It computes at each leaf node the inner product (hence, mass matrix) of the
     * local basis function. It determines the local index and updates the
     * corresponding entry of the local matrix.
     */
    template <class LV, class M>
    class LeafNodeMassAssembler
    {
    public:

      using LocalView = LV;
      using Matrix = M;
      using field_type = typename M::field_type;

      /** \brief Constructor with all necessary context information
       *
       * \param [in] lv (localView) element information and localBasis
       * \param [out] m (matrix) resulting local mass matrix, wrapped in the ISTLBackend

       */
      LeafNodeMassAssembler(const LV& lv, M& m)
      : localView_(lv)
      , matrix_(m)
      {}

      template<class Node, class TreePath>
      void operator()(Node& node, TreePath treePath)
      {
        const auto& element = localView_.element();
        const auto& geometry = element.geometry();
        const auto& finiteElement = node.finiteElement();
        const auto& localBasis = finiteElement.localBasis();
        using RangeType = typename std::decay_t<decltype(localBasis)>::Traits::RangeType;
        constexpr int gridDim = LocalView::GridView::dimension;

        std::vector<RangeType> values(localBasis.size());

        QuadratureRuleKey quadKey(finiteElement);
        const auto& quad = QuadratureRuleCache<double, gridDim>::rule(quadKey);

        for( const auto& qp : quad )
        {
          const auto& quadPos = qp.position();
          const auto integrationElement = geometry.integrationElement(quadPos);

          localBasis.evaluateFunction(quadPos, values);

          // compute matrix entries = inner products
          auto z = qp.weight() * integrationElement;
          for (std::size_t i=0; i<localBasis.size(); ++i)
          {
            auto zi = values[i]*z;

            auto rowIndex = node.localIndex(i);

            // start off-diagonal, treat the diagonal extra
            for (std::size_t j=i+1; j<localBasis.size(); ++j)
            {
              auto zij = values[j] * zi;

              auto colIndex = node.localIndex(j);

              // mass matrix is symmetric
              matrix_[rowIndex][colIndex] += zij;
              matrix_[colIndex][rowIndex] += zij;
            }

            // z * values[i]^2
            matrix_[rowIndex][rowIndex] += values[i] * zi;
          }
        }
      }

    private:
      const LocalView& localView_;
      Matrix& matrix_;
    };

  } // namespace Impl

  /** \brief Local mass assembler for dune-functions basis
   *
   * We assume the mass matrix to be symmetric and hence ansatz and trial space to be equal!
   * The implementation allows an arbitrary dune-functions compatible basis, as long as there
   * is an ISTLBackend available for the resulting matrix.
   */
  class MassAssembler
  {
  public:

    template<class Element, class LocalMatrix, class LocalView>
    void operator()(const Element& element, LocalMatrix& localMatrix, const LocalView& trialLocalView, const LocalView& ansatzLocalView) const
    {
      // the mass matrix operator assumes trial == ansatz
      if (&trialLocalView != &ansatzLocalView)
        DUNE_THROW(Dune::Exception,"The mass matrix operator assumes equal ansatz and trial functions");

      // matrix was already resized before
      localMatrix = 0.;

      // create a tree visitor and compute the inner products of the local functions at the leaf nodes
      Impl::LeafNodeMassAssembler leadNodeMassAssembler(trialLocalView,localMatrix);
      TypeTree::forEachLeafNode(trialLocalView.tree(),leadNodeMassAssembler);
    }

  };

} // namespace Dune::Fufem

/** \brief Local mass assembler

 * \deprecated This assembler uses the old dune-fufem function space bases.  Please migrate
   to the assembler based on dune-functions bases (above).
**/
template <class GridType, class TrialLocalFE, class AnsatzLocalFE, class T=Dune::FieldMatrix<double,1,1> >
class MassAssembler : public LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, T >
{
    private:
        typedef LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE ,T > Base;
        static const int dim = GridType::dimension;
        const int quadOrder_;


    public:
        typedef typename Base::Element Element;
        typedef typename Element::Geometry Geometry;
        typedef typename Base::BoolMatrix BoolMatrix;
        typedef typename Base::LocalMatrix LocalMatrix;

        MassAssembler():
            quadOrder_(-1)
        {}

        DUNE_DEPRECATED_MSG("Quadrature order is now selected automatically. you don't need to specify it anymore.")
        MassAssembler(int quadOrder):
            quadOrder_(quadOrder)
        {}

        void indices(const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            isNonZero = true;
        }

        template <class BoundaryIterator>
        void indices(const BoundaryIterator& it, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            isNonZero = true;
        }

        void assemble(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename Dune::template FieldVector<double,dim> FVdim;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType RangeType;

            // Make sure we got suitable shape functions
            assert(tFE.type() == element.type());
            assert(aFE.type() == element.type());

            // check if ansatz local fe = test local fe
//            if (not Base::isSameFE(tFE, aFE))
//                DUNE_THROW(Dune::NotImplemented, "MassAssembler is only implemented for ansatz space=test space!");

            int rows = localMatrix.N();
            int cols = localMatrix.M();

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localMatrix = 0.0;

            // get quadrature rule
            QuadratureRuleKey quadKey = QuadratureRuleKey(tFE)
                .product(QuadratureRuleKey(aFE));
            if (quadOrder_>=0)
                quadKey.setOrder(quadOrder_);
            const Dune::template QuadratureRule<double, dim>& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

            // store values of shape functions
            std::vector<RangeType> values(tFE.localBasis().size());

            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const FVdim& quadPos = quad[pt].position();

                // get integration factor
                const double integrationElement = geometry.integrationElement(quadPos);

                // evaluate basis functions
                tFE.localBasis().evaluateFunction(quadPos, values);

                // compute matrix entries
                double z = quad[pt].weight() * integrationElement;
                for(int i=0; i<rows; ++i)
                {
                    auto zi = values[i]*z;

                    for (int j=i+1; j<cols; ++j)
                    {
                        auto zij = values[j] * zi;
                        Dune::MatrixVector::addToDiagonal(localMatrix[i][j],zij);
                        Dune::MatrixVector::addToDiagonal(localMatrix[j][i],zij);
                    }
                    Dune::MatrixVector::addToDiagonal(localMatrix[i][i], values[i] * zi);
                }
            }
        }


        /** \brief Assemble the local mass matrix for a given boundary face
         */
        template <class BoundaryIterator>
        void assemble(const BoundaryIterator& it, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename BoundaryIterator::Intersection::Geometry Geometry;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType RangeType;

            // Make sure we got suitable shape functions
            assert(tFE.type() == it->inside().type());
            assert(aFE.type() == it->inside().type());

            // check if ansatz local fe = test local fe
            if (not Base::isSameFE(tFE, aFE))
                DUNE_THROW(Dune::NotImplemented, "MassAssembler is only implemented for ansatz space=test space!");

            int rows = localMatrix.N();
            int cols = localMatrix.M();

            // get geometry and store it
            const Geometry intersectionGeometry = it->geometry();

            localMatrix = 0.0;

            // get quadrature rule
            QuadratureRuleKey tFEquad(it->type(), tFE.localBasis().order());
            QuadratureRuleKey quadKey = tFEquad.square();

            const Dune::template QuadratureRule<double, dim-1>& quad = QuadratureRuleCache<double, dim-1>::rule(quadKey);

            // store values of shape functions
            std::vector<RangeType> values(tFE.localBasis().size());

            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const Dune::FieldVector<double,dim-1>& quadPos = quad[pt].position();

                // get integration factor
                const double integrationElement = intersectionGeometry.integrationElement(quadPos);

                // get values of shape functions
                tFE.localBasis().evaluateFunction(it->geometryInInside().global(quadPos), values);

                // compute matrix entries
                double z = quad[pt].weight() * integrationElement;
                for (int i=0; i<rows; ++i)
                {
                    for (int j=i+1; j<cols; ++j)
                    {
                        double zij = values[i] * values[j] * z;
                        Dune::MatrixVector::addToDiagonal(localMatrix[i][j],zij);
                        Dune::MatrixVector::addToDiagonal(localMatrix[j][i],zij);
                    }
                    Dune::MatrixVector::addToDiagonal(localMatrix[i][i], values[i] * values[i] * z);
                }
            }

        }

};


#endif

