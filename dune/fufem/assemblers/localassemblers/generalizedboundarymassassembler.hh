// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_GENERALIZED_BOUNDARY_MASS_ASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_GENERALIZED_BOUNDARY_MASS_ASSEMBLER_HH

#include <vector>

#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>

/** \brief Local Robin boundary assembler
 * 
 * \tparam GridType The grid we are assembling for
 * \tparam The trial local finite element type
 * \tparam The ansatz local finite element type
 * \tparam The function for evaluating the coefficient
 * \tparam T Type used for the set of dofs at one node
 */
template <class GridType, class TrialLocalFE, class AnsatzLocalFE, class Function, class T=Dune::FieldMatrix<typename GridType::ctype,1,1>>
class GeneralizedBoundaryMassAssembler
{
    private:
        static const int dim = GridType::dimension;
        typedef typename GridType::ctype ctype;

    public:
        typedef Dune::Matrix< Dune::FieldMatrix<int,1,1> > BoolMatrix;
        typedef typename Dune::Matrix<T> LocalMatrix;
        typedef typename T::field_type field_type;
        typedef VirtualGridFunction<GridType,typename Function::RangeType> GridFunction;

        /** \brief Constructor
         * \param order The quadrature order used for numerical integration
         */
         
        GeneralizedBoundaryMassAssembler(const Function& robin, int order=2) :
            robin_(robin),
            order_(order)
        {}

        template<class BoundaryIterator>
        void indices (const BoundaryIterator& it, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) 
        {
          isNonZero = true;
        }
        
        template<class BoundaryIterator>
        void assemble(const BoundaryIterator& it, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType RangeType;

            localMatrix = 0.0;

            // geometry of the boundary face
            const auto segmentGeometry = it->geometry();


            // get quadrature rule
            const auto& quad = QuadratureRuleCache<ctype, dim-1>::rule(segmentGeometry.type(), order_, IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE) );

            // store values of shape functions
            std::vector<RangeType> tFEvalues(tFE.localBasis().size());
            std::vector<RangeType> aFEvalues(aFE.localBasis().size());

            const auto& geometryInInside = it->geometryInInside();

            // loop over quadrature points
            for (const auto& pt : quad)
            {
                const auto inside = it->inside();

                // get quadrature point
                const auto& quadPos = pt.position();

                // get integration factor
                const auto integrationElement = segmentGeometry.integrationElement(quadPos);

                // position of the quadrature point within the element
                const auto elementQuadPos = geometryInInside.global(quadPos);

                // evaluate basis functions
                tFE.localBasis().evaluateFunction(elementQuadPos, tFEvalues);
                aFE.localBasis().evaluateFunction(elementQuadPos, aFEvalues);

                // Evaluate robin function at quadrature point. If it is a grid function use that to speed up the evaluation
                typename Function::RangeType robinVal; 
                const GridFunction* gf = dynamic_cast<const GridFunction*>(&robin_);
                if (gf and gf->isDefinedOn(inside))
                    gf->evaluateLocal(inside, elementQuadPos, robinVal);
                else
                    robin_.evaluate(segmentGeometry.global(quadPos), robinVal);
                
                // compute matrix entries
                double z = pt.weight()*integrationElement;
                                
                for (size_t i=0; i<tFEvalues.size(); ++i)
                {
                    ctype zi = tFEvalues[i]*z;
                    for (size_t j = 0; j<aFEvalues.size(); ++j) {
                        auto Mij = robinVal;
                        Mij *= aFEvalues[j]*zi;
                        localMatrix[i][j] += Mij;
                    }
                }
            }
            return;
        }

    private:
        const Function& robin_;
        // quadrature order
        const int order_;
};

#endif

