// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_ADOLC_LINEARIZATION_ASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_ADOLC_LINEARIZATION_ASSEMBLER_HH

#include <dune/common/fvector.hh>
#include <dune/fufem/assemblers/localfunctionalassembler.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>
#include <dune/fufem/assemblers/localassemblers/adolclocalenergy.hh>


/** \brief Local finite element assembler that computes the exact linearization of an energy functional
 *      using the automatic differentiation library ADOL-C. Needs a local energy that implements the 
 *      Adolc::LocalEnergy interface.
 * */ 
template <class GridType, class TrialLocalFE, class T=Dune::FieldVector<double,1> >
class AdolcLinearizationAssembler :
  public LocalFunctionalAssembler<GridType, TrialLocalFE, T>
{
  private:
    typedef LocalFunctionalAssembler<GridType, TrialLocalFE, T> Base;

    enum {blocksize=T::dimension};

    typedef Adolc::LocalEnergy<GridType, TrialLocalFE,blocksize> LocalEnergy;

    typedef typename T::field_type field_type;
    typedef std::vector<T> CoefficientVectorType;

    typedef VirtualGridFunction<GridType, T> GridFunction;

    typedef typename TrialLocalFE::Traits::LocalBasisType::Traits FunctionTraits;
    typedef LocalFunctionComponentWrapper<GridFunction, GridType, FunctionTraits> LocalWrapper;

  public:
    typedef typename Base::Element Element;
    typedef typename Base::Element::Geometry Geometry;
    typedef typename Base::LocalVector LocalVector;

    /** \brief Create LinearizationAssembler */
    AdolcLinearizationAssembler() {}

    /**
     * \brief Create LinearizationAssembler
     *
     * Creates a local assembler that computes the exact linearization of a
     * given energy functional using the automatic differentiation library Adol-C
     *
     * \param energy The energy functional
     * \param configuration The point at which the linearization is computed
     *
     */
    AdolcLinearizationAssembler(const LocalEnergy& energy, const GridFunction& configuration) :
        energy_(&energy),
        configuration_(&configuration)
    {}

    /**
     * \brief Create LinearizationAssembler
     *
     * Creates a local assembler that computes the exact linearization of a
     * given energy functional using the automatic differentiation library Adol-C
     *
     * \param energy The energy functional
     */
    AdolcLinearizationAssembler(const LocalEnergy& energy) :
        energy_(&energy)
    {}

    //! Assembler the local linearization
    void assemble(const Element& element, LocalVector& localVector, const TrialLocalFE& tFE) const
    { 

        // interpolate by local finite element to get the local coefficients    
        CoefficientVectorType localCoeff(tFE.localBasis().size());
        LocalWrapper fiLocal(*configuration_,element,0);

        std::vector<typename LocalWrapper::RangeType> interpolationValues;
        for (int i=0; i<blocksize; i++) { 
            fiLocal.setIndex(i);

            tFE.localInterpolation().interpolate(fiLocal,interpolationValues);
            for (size_t j=0; j<tFE.localBasis().size(); j++)
                localCoeff[j][i] = interpolationValues[j];
        }

        // call assemble method
        assemble(element,localCoeff,localVector,tFE);
    }

    /** \brief Set configuration at which point the energy is linearized. */
    void setConfiguration(const GridFunction& configuration)
    {
        configuration_ = &configuration;
    }

    /** \brief Set configuration at which point the energy is linearized. */
    void setConfiguration(std::shared_ptr<GridFunction> configuration)
    {
        configuration_ = configuration.get();
    }

  private:
    //! Assembler the local linearization
    void assemble(const Element& element, const CoefficientVectorType& localCoeff,
                  LocalVector& localVector, const TrialLocalFE& tFE) const
    {
        localVector = 0.0;

        // Tape energy computation.  
        // We may not have to do this every time, but it's comparatively cheap.
        Adolc::tapeEnergy(element, tFE, localCoeff,energy_);

        /////////////////////////////////
        //  Compute the linearisation
        /////////////////////////////////

        // Copy data from Dune data structures to plain-C ones
        size_t nDofs = localCoeff.size();
        size_t nDoubles = nDofs*blocksize;
        std::vector<field_type> xp(nDoubles);
        int idx=0;
        for (size_t i=0; i<nDofs; i++)
            for (size_t j=0; j<blocksize; j++)
                xp[idx++] = localCoeff[i][j];

        // Compute gradient
        std::vector<field_type> g(nDoubles);
        gradient(1,nDoubles,xp.data(),g.data());// gradient evaluation

        // Copy into Dune type
        idx=0;
        for (size_t i=0; i<nDofs; i++)
            for (size_t j=0; j<blocksize; j++)
                localVector[i][j] = g[idx++];
        return;
    }

    //! The energy functional that is to be linearized
    const LocalEnergy* energy_;

    //! Grid function representing the configuration at which the energy is linearized
    const GridFunction* configuration_;
};

#endif

