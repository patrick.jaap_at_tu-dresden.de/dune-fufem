#ifndef LAPLACE_ASSEMBLER_HH
#define LAPLACE_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/transpose.hh>

#include <dune/istl/matrix.hh>

#include <dune/matrix-vector/addtodiagonal.hh>

#include <dune/typetree/visitor.hh>
#include <dune/typetree/traversal.hh>

#include "dune/fufem/quadraturerules/quadraturerulecache.hh"

#include "dune/fufem/assemblers/localoperatorassembler.hh"


namespace Dune::Fufem
{

  namespace Impl
  {

    /** \brief Computes the inner product of the jacobians of the local basis functions
     *
     * It computes at each leaf node the inner product (hence, stiffness matrix) of the
     * jacobians of the local basis functions. It determines the local index and updates the
     * corresponding entry of the local matrix.
     */
    template <class LV, class M>
    class LeafNodeLaplaceAssembler
    {
    public:

      using LocalView = LV;
      using Matrix = M;
      using field_type = typename M::field_type;

      /** \brief Constructor with all necessary context information
       *
       * \param [in] lv (localView) element information and localBasis
       * \param [out] m (matrix) resulting local stiffness matrix, wrapped in the ISTLBackend

       */
      LeafNodeLaplaceAssembler(const LV& lv, M& m)
      : localView_(lv)
      , matrix_(m)
      {}

      template<class Node, class TreePath>
      void operator()(Node& node, [[maybe_unused]] TreePath treePath)
      {
        const auto& element = localView_.element();
        const auto& geometry = element.geometry();
        const auto& finiteElement = node.finiteElement();
        const auto& localBasis = finiteElement.localBasis();
        using RangeType = typename std::decay_t<decltype(localBasis)>::Traits::RangeType;
        using JacobianType = typename std::decay_t<decltype(localBasis)>::Traits::JacobianType;
        constexpr int gridDim = LocalView::GridView::dimension;

        QuadratureRuleKey quadKey(finiteElement);
        const auto& quad = QuadratureRuleCache<double, gridDim>::rule(quadKey);

        for( const auto& qp : quad )
        {
          const auto& quadPos = qp.position();
          const auto integrationElement = geometry.integrationElement(quadPos);

          std::vector<JacobianType> referenceJacobians;;
          localBasis.evaluateJacobian(quadPos, referenceJacobians);

          const auto& jacobianInverseTransposed = geometry.jacobianInverseTransposed(quadPos);

          // transform jacobians
          std::vector<JacobianType> jacobians(referenceJacobians.size());
          for (size_t i=0; i<jacobians.size(); i++)
            jacobians[i] = referenceJacobians[i] * transpose(jacobianInverseTransposed);

          // compute matrix entries = inner products of jacobians
          auto z = qp.weight() * integrationElement;
          for (std::size_t i=0; i<localBasis.size(); ++i)
          {
            auto zi = z*jacobians[i];

            auto rowIndex = node.localIndex(i);

            // start off-diagonal, treat the diagonal extra
            for (std::size_t j=i+1; j<localBasis.size(); ++j)
            {
              auto colIndex = node.localIndex(j);

              // we need the Frobenius inner product here, since the matrix may be fully occupied (Raviart-Thomas, etc)
              // I could not find an implementation anywhere in DUNE, therefore this ugly loop:
              for ( std::size_t ii=0; ii<zi.N(); ii++ )
              {
                for ( std::size_t jj=0; jj<zi.M(); jj++ )
                {
                  // mass matrix is symmetric
                  auto zij = zi[ii][jj] * jacobians[j][ii][jj];
                  matrix_[rowIndex][colIndex] += zij;
                  matrix_[colIndex][rowIndex] += zij;
                }
              }
            }

            // z * values[i]^2
            for ( std::size_t ii=0; ii<zi.N(); ii++ )
              for ( std::size_t jj=0; jj<zi.M(); jj++ )
                matrix_[rowIndex][rowIndex] += zi[ii][jj] * jacobians[i][ii][jj];
          }
        }
      }

    private:
      const LocalView& localView_;
      Matrix& matrix_;
    };

  } // namespace Impl

  /** \brief Local Laplace (stiffness) assembler for dune-functions basis
   *
   * We assume the stiffness matrix to be symmetric and hence ansatz and trial space to be equal!
   * The implementation allows an arbitrary dune-functions compatible basis, as long as there
   * is an ISTLBackend available for the resulting matrix.
   */
  class LaplaceAssembler
  {
  public:

    template<class Element, class LocalMatrix, class LocalView>
    void operator()(const Element& element, LocalMatrix& localMatrix, const LocalView& trialLocalView, const LocalView& ansatzLocalView) const
    {
      // the Laplace matrix operator assumes trial == ansatz
      if (&trialLocalView != &ansatzLocalView)
        DUNE_THROW(Dune::Exception,"The Laplace matrix operator assumes equal ansatz and trial functions");

      // matrix was already resized before
      localMatrix = 0.;

      // create a tree visitor and compute the inner products of the local functions at the leaf nodes
      Impl::LeafNodeLaplaceAssembler leadNodeLaplaceAssembler(trialLocalView,localMatrix);
      TypeTree::forEachLeafNode(trialLocalView.tree(),leadNodeLaplaceAssembler);
    }

  };

} // namespace Dune::Fufem


/** \brief Local assembler for the Laplace problem

  * \deprecated This assembler uses the old dune-fufem function space bases.  Please migrate
                to the assembler based on dune-functions bases (above).
*/
template <class GridType, class TrialLocalFE, class AnsatzLocalFE, class T=Dune::FieldMatrix<double,1,1> >
class LaplaceAssembler : public LocalOperatorAssembler <GridType, TrialLocalFE, AnsatzLocalFE, T >
{
    private:
        typedef LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE ,T > Base;
        static const int dim      = GridType::dimension;
        static const int dimworld = GridType::dimensionworld;
        const int quadOrder_;

        /** \brief Construct an orthonormal basis using the Gram-Schmidt algorithm
            \param basis The input basis; each matrix row is a basis vector
         */
        static Dune::FieldMatrix<double,dim-1,dim> makeOrthonormal(const Dune::FieldMatrix<double,dim-1,dim>& basis)
        {
            Dune::FieldMatrix<double,dim-1,dim> orthonormalBasis(basis);

            for (int i=0; i<dim-1; i++) {

                orthonormalBasis[i] /= orthonormalBasis[i].two_norm();

                for (int j=0; j<i; j++)
                    // project orthonormalBasis[j] onto orthonormalBasis[i], and subtract the result from orthonormalBasis[j]
                    orthonormalBasis[j].axpy(-(orthonormalBasis[i] * orthonormalBasis[j]), orthonormalBasis[i]);

            }

            return orthonormalBasis;
        }

    public:
        typedef typename Base::Element Element;
        typedef typename Element::Geometry Geometry;
        typedef typename Geometry::JacobianInverseTransposed JacobianInverseTransposed;
        typedef typename Base::BoolMatrix BoolMatrix;
        typedef typename Base::LocalMatrix LocalMatrix;

        LaplaceAssembler() :
            quadOrder_(-1)
        {}

        DUNE_DEPRECATED_MSG("Quadrature order is now selected automatically. you don't need to specify it anymore.")
        LaplaceAssembler(int quadOrder) :
            quadOrder_(quadOrder)
        {}

        void indices(const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            isNonZero = true;
        }

        template <class BoundaryIterator>
        void indices(const BoundaryIterator& it, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            isNonZero = true;
        }

        /** \brief Assemble the local stiffness matrix for a given element
         */
        void assemble(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename Dune::template FieldVector<double,dim> FVdim;
            typedef typename Dune::template FieldVector<double,dimworld> FVdimworld;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;

            // Make sure we got suitable shape functions
            assert(tFE.type() == element.type());
            assert(aFE.type() == element.type());

            // check if ansatz local fe = test local fe
//            if (not Base::isSameFE(tFE, aFE))
//                DUNE_THROW(Dune::NotImplemented, "LaplaceAssembler is only implemented for ansatz space=test space!");

            int rows = localMatrix.N();
            int cols = localMatrix.M();

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localMatrix = 0.0;

            // get quadrature rule
            QuadratureRuleKey tFEquad(tFE);
            QuadratureRuleKey quadKey = tFEquad.derivative().square();
            if (quadOrder_>=0)
                quadKey.setOrder(quadOrder_);
            const Dune::template QuadratureRule<double, dim>& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

            // store gradients of shape functions and base functions
            std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
            std::vector<FVdimworld> gradients(tFE.localBasis().size());


            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const FVdim& quadPos = quad[pt].position();

                // get transposed inverse of Jacobian of transformation
                const JacobianInverseTransposed& invJacobian = geometry.jacobianInverseTransposed(quadPos);

                // get integration factor
                const double integrationElement = geometry.integrationElement(quadPos);

                // get gradients of shape functions
                tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

                // transform gradients
                for (size_t i=0; i<gradients.size(); ++i)
                    invJacobian.mv(referenceGradients[i][0], gradients[i]);

                // compute matrix entries
                double z = quad[pt].weight() * integrationElement;
                for (int i=0; i<rows; ++i)
                {
                    for (int j=i+1; j<cols; ++j)
                    {
                        double zij = (gradients[i] * gradients[j]) * z;
                        Dune::MatrixVector::addToDiagonal(localMatrix[i][j],zij);
                        Dune::MatrixVector::addToDiagonal(localMatrix[j][i],zij);
                    }
                    Dune::MatrixVector::addToDiagonal(localMatrix[i][i], (gradients[i] * gradients[i]) * z);
                }
            }
            return;
        }


        /** \brief Assemble the local stiffness matrix for a given boundary face
         */
        template <class BoundaryIterator>
        void assemble(const BoundaryIterator& it, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename Dune::template FieldVector<double,dimworld> FVdimworld;

            typedef typename BoundaryIterator::Intersection::Geometry Geometry;
            typedef typename BoundaryIterator::Intersection::Entity Entity;
            typedef typename Entity::Geometry InsideGeometry;
            typedef typename InsideGeometry::JacobianInverseTransposed JacobianInverseTransposed;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;

            Entity const inside = it->inside();

            // Make sure we got suitable shape functions
            assert(tFE.type() == inside.type());
            assert(aFE.type() == inside.type());

            // check if ansatz local fe = test local fe
            if (not Base::isSameFE(tFE, aFE))
                DUNE_THROW(Dune::NotImplemented, "LaplaceAssembler is only implemented for ansatz space=test space!");

            int rows = localMatrix.N();
            int cols = localMatrix.M();

            // get geometry and store it
            const Geometry intersectionGeometry = it->geometry();
            const InsideGeometry insideGeometry = inside.geometry();

            localMatrix = 0.0;

            // get quadrature rule
            QuadratureRuleKey tFEquad(it->type(), tFE.localBasis().order());
            QuadratureRuleKey quadKey = tFEquad.derivative().square();

            const Dune::template QuadratureRule<double, dim-1>& quad = QuadratureRuleCache<double, dim-1>::rule(quadKey);

            // store gradients of shape functions and base functions
            std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
            std::vector<FVdimworld> gradients(tFE.localBasis().size());


            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const Dune::FieldVector<double,dim-1>& quadPos = quad[pt].position();

                // get transposed inverse of Jacobian of transformation
                const JacobianInverseTransposed& invJacobian = insideGeometry.jacobianInverseTransposed(it->geometryInInside().global(quadPos));

                // get integration factor
                const double integrationElement = intersectionGeometry.integrationElement(quadPos);

                // get gradients of shape functions
                tFE.localBasis().evaluateJacobian(it->geometryInInside().global(quadPos), referenceGradients);

                // Project gradient on the inside element tangent plane, to obtain
                // the correct surface gradient
                // Step 1: get an orthonormal basis of the tangent space at the current quadrature point
                typename GridType::template Codim<1>::LocalGeometry::JacobianTransposed tangentSpaceBasis = it->geometryInInside().jacobianTransposed(quadPos);
                Dune::FieldMatrix<double,dim-1,dim> orthonormalBasis = makeOrthonormal(tangentSpaceBasis);

                // Step 2: project
                std::vector<Dune::FieldVector<double,dim> > projectedReferenceGradients(tFE.localBasis().size());
                for (size_t i=0; i<projectedReferenceGradients.size(); i++) {
                    projectedReferenceGradients[i] = 0;
                    for (size_t j=0; j<dim-1; j++)
                        projectedReferenceGradients[i].axpy(referenceGradients[i][0]*orthonormalBasis[j],orthonormalBasis[j]);
                }

                // transform gradients
                for (size_t i=0; i<gradients.size(); ++i)
                    invJacobian.mv(projectedReferenceGradients[i], gradients[i]);

                // compute matrix entries
                double z = quad[pt].weight() * integrationElement;
                for (int i=0; i<rows; ++i)
                {
                    for (int j=i+1; j<cols; ++j)
                    {
                        double zij = (gradients[i] * gradients[j]) * z;
                        Dune::MatrixVector::addToDiagonal(localMatrix[i][j],zij);
                        Dune::MatrixVector::addToDiagonal(localMatrix[j][i],zij);
                    }
                    Dune::MatrixVector::addToDiagonal(localMatrix[i][i], (gradients[i] * gradients[i]) * z);
                }
            }

        }

};


#endif

