#ifndef DUNE_FUFEM_FORMATSTRING_HH
#define DUNE_FUFEM_FORMATSTRING_HH

#include <cstdio>

#include <dune/common/exceptions.hh>

namespace Dune {
namespace Fufem {

/**
 * \brief Return formated output for printf like string
 *
 * \param s Format string using printf syntax
 * \param args Arguments for format string
 * \returns Result of conversion as string
 */
template<class... T>
std::string formatString(const std::string& s, const T&... args)
{
    static const int bufferSize=1000;
    char buffer[bufferSize];

    int r = std::snprintf(buffer, bufferSize, s.c_str(), args...);

    // negative return values correspond to errors
    if (r<0)
        DUNE_THROW(Dune::Exception,"Could not convert format string using given arguments.");

    // if buffer was large enough return result as string
    if (r<bufferSize)
        return std::string(buffer);

    // if buffer was to small allocate a larger buffer using
    // the returned size hint +1 for the terminating 0-byte.
    int dynamicBufferSize = r+1;
    char* dynamicBuffer = new char[dynamicBufferSize];

    // convert and check for errors again
    r = std::snprintf(dynamicBuffer, dynamicBufferSize, s.c_str(), args...);
    if (r<0)
        DUNE_THROW(Dune::Exception,"Could not convert format string using given arguments.");

    // the new buffer should always be large enough
    assert(r<dynamicBufferSize);

    // convert result to string, release buffer, return result
    std::string result(dynamicBuffer);
    delete[] dynamicBuffer;

    return result;
}

}
}

#endif // DUNE_FUFEM_FORMATSTRING_HH
