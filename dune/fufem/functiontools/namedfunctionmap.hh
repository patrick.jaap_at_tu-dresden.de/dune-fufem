#ifndef NAMED_FUNCTION_MAP_HH
#define NAMED_FUNCTION_MAP_HH

#include <map>
#include <string>

#include <dune/common/exceptions.hh>
#include <dune/common/function.hh>
#include <dune/fufem/functions/virtualdifferentiablefunction.hh>

template <class DomainType, class RangeType>
class NamedFunctionMap :
    public std::map<std::string, typename Dune::VirtualFunction<DomainType,RangeType>* >
{
    public:
        typedef typename Dune::VirtualFunction<DomainType,RangeType> Function;
        typedef VirtualDifferentiableFunction<DomainType,RangeType> DifferentiableFunction;

        NamedFunctionMap()
        {
        }

        const Function* getFunction(const std::string& name) const
        {
            typename Base::const_iterator it = this->find(name);
            if (it!=this->end())
                return it->second;
            DUNE_THROW(Dune::RangeError, "Function named '" << name << "' not found in NamedFunctionMap!");
            return 0;
        }

        Function* getFunction(const std::string& name)
        {
            typename Base::const_iterator it = this->find(name);
            if (it!=this->end())
                return it->second;
            DUNE_THROW(Dune::RangeError, "Function named '" << name << "' not found in NamedFunctionMap!");
            return 0;
        }

        const DifferentiableFunction* getDifferentiableFunction(const std::string& name) const
        {
            const Function* p = getFunction(name);
            return dynamic_cast<const DifferentiableFunction*>(getFunction(name));
        }

        DifferentiableFunction* getDifferentiableFunction(const std::string& name)
        {
            Function* p = getFunction(name);
            return dynamic_cast<DifferentiableFunction*>(getFunction(name));
        }


    private:
        typedef std::map<std::string, Function* > Base;
};


#endif
