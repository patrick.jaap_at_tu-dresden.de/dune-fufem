#ifndef GRADIENT_BASIS_HH
#define GRADIENT_BASIS_HH

/**
   @file gradientbasis.hh
   @brief Provide struct to generate a basis type that can represent gradients of a given basis

   @author graeser@math.fu-berlin.de
 */

#include <dune/fufem/functionspacebases/p0basis.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/functionspacebases/refinedp0basis.hh>
#include <dune/fufem/functionspacebases/refinedp1nodalbasis.hh>
#include <dune/fufem/functionspacebases/conformingbasis.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

/** \brief Provides a typedef for a basis type that can represent gradients of the given basis
 *
 * \tparam Basis Consider gradients of functions form this basis
 *
 */
template <class Basis>
struct GradientBasis
{};

template <class NonconformingBasis>
struct GradientBasis<ConformingBasis<NonconformingBasis> >
{
    typedef typename GradientBasis<NonconformingBasis>::type type;
};

template <class GridView>
struct GradientBasis< P1NodalBasis<GridView> >
{
    typedef P0Basis<GridView> type;
};

template <class GridView>
struct GradientBasis<
    DuneFunctionsBasis<Dune::Functions::LagrangeBasis<GridView, 1>>> {
  typedef DuneFunctionsBasis<Dune::Functions::LagrangeBasis<GridView, 0>> type;
};

template <class GridView>
struct GradientBasis< RefinedP1NodalBasis<GridView> >
{
    typedef RefinedP0Basis<GridView> type;
};

template<class Basis>
using GradientBasis_t = typename GradientBasis<Basis>::type;

#endif

