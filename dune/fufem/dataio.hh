#ifndef DATAIO_HH
#define DATAIO_HH


#ifdef HAVE_BOOST_SERIALIZATION

#include <fstream>

#include <dune/common/exceptions.hh>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>


/** \brief provides write/load functionality for (boost::) serializable data containers
  *
  *  Remember to add -lboost_serialization to your linker flags LDADD if you want to use this class.
  */
class DataIO
{
    public:
        //! supported file formats
        enum FileFormat { BINARY, ASCII, XML };

        /**  \brief writes (boost::) serializable data containers
          *
          *  \tparam DataContainer some type providing a serialize function conforming to boost::serialization requirements
          *
          *  \param data the data to be written/serialized
          *  \param filename the filename for the output file
          *  \param format the file format of the output file
          */
        template <class DataContainer>
        static void writeData(const DataContainer& data, std::string filename, FileFormat format=BINARY)
        {
            std::ofstream ofs(filename.c_str());


            switch (format) {
                case BINARY:
                {
                    boost::archive::binary_oarchive oa(ofs);
                    oa << data;
                    break;
                }
                case ASCII:
                {
                    boost::archive::text_oarchive oa(ofs);
                    oa << data;
                    break;
                }
                case XML:
                    DUNE_THROW(Dune::NotImplemented, "Serialized output in XML format not yet implemented.");
            }
        }

        /**  \brief reads data previously written via boost::serialization into given data containers
          *
          *  \tparam DataContainer some type providing a serialize function conforming to boost::serialization requirements
          *
          *  \param data the data to be read
          *  \param filename the filename of the input file
          *  \param format the file format of the input file
          */
        template <class DataContainer>
        static void loadData(DataContainer& data, std::string filename, FileFormat format=BINARY)
        {
            std::ifstream ifs(filename.c_str());

            switch (format) {
                case BINARY:
                {
                    boost::archive::binary_iarchive ia(ifs);
                    ia >> data;
                    break;
                }
                case ASCII:
                {
                    boost::archive::text_iarchive ia(ifs);
                    ia >> data;
                    break;
                }
                case XML:
                    DUNE_THROW(Dune::NotImplemented, "Serialized output in XML format not yet implemented.");
            }
        }
};

#else
#warning dataio.hh was included but boost_serialization was not found !
#endif

#endif

