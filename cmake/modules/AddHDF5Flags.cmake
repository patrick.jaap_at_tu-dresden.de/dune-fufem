#
# Module providing convenience methods for compile binaries with HDF5 support.
#
# Provides the following functions:
#
# add_dune_hdf5_flags(target1 target2 ...)
#
# adds HDF5 flags to the targets for compilation and linking
#

function(add_dune_hdf5_flags _targets)
    if(HDF5_FOUND)
        foreach(_target ${_targets})
            target_link_libraries(${_target} ${HDF5_LIBRARIES} ${HDF5_HL_LIBRARIES})
            set_property(TARGET ${_target} APPEND PROPERTY INCLUDE_DIRECTORIES ${HDF5_INCLUDE_DIRS})
        endforeach(_target ${_targets})
    endif(HDF5_FOUND)
endfunction(add_dune_hdf5_flags)
